# 우즈Tech Project

Welcome to the 우즈Tech project! This project is based on the Zen Kernel and is developed and maintained by DaeBox Studio. !!NAME SUBJECT TO CHANGE!!

## Contributing

We welcome bug reports and fixes from all users. However, only members of DaeBox Studio are allowed to modify the code directly. If you're not a member and you'd like to contribute a fix, please see our CONTRIBUTING guide for details on how to submit a pull request.

## License

This project is licensed under the terms of the 우즈Tech Project License. You can see the full license terms in the file LICENSE.txt.

## Miscellaenous

As a dev you are REQUIRED to obtain a license file. Please contact the discord admins/owners for assistance.
You can aslo request to become a dev from Discord.
