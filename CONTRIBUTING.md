# Contributing to ChilBoraGalaxy Project

## Bug Reports

- Please submit bug reports through the issue tracker on our GitHub repository.
- Be as detailed as possible to help us understand and reproduce the issue.

## Fixes and Enhancements

- If you're not a member of DaeBox Studio and you'd like to contribute a fix or enhancement, please fork our repository, make your changes in your fork, and then submit a pull request.
- Please describe your changes in detail in the pull request.

## Code Standards

- Please follow the coding standards used throughout the project.
- Keep your code clean and well-commented.

Thank you for your interest in contributing to the UjuTech project!
